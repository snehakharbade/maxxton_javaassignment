package com.assignment.date;

import java.time.LocalDate;
import java.util.Comparator;

public class DateRange {
	private LocalDate start_date;
	private LocalDate end_date;
	
	public DateRange() {
		
	}
	
	public DateRange( LocalDate start_date, LocalDate end_date) {
		this.start_date = start_date;
		this.end_date = end_date;
	}

	public LocalDate getStart_date() {
		return start_date;
	}

	public void setStart_date(LocalDate start_date) {
		this.start_date = start_date;
	}

	public LocalDate getEnd_date() {
		return end_date;
	}

	public void setEnd_date(LocalDate end_date) {
		this.end_date = end_date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end_date == null) ? 0 : end_date.hashCode());
		result = prime * result + ((start_date == null) ? 0 : start_date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateRange other = (DateRange) obj;
		if (end_date == null) {
			if (other.end_date != null)
				return false;
		} else if (!end_date.equals(other.end_date))
			return false;
		if (start_date == null) {
			if (other.start_date != null)
				return false;
		} else if (!start_date.equals(other.start_date))
			return false;
		return true;
	}
	
	public static final Comparator<DateRange> start_date_Comparator = (DateRange d1,DateRange d2) ->{
		if (d1.getStart_date() != null && d2.getStart_date() != null) {
		      if (d1.getStart_date().isBefore(d2.getStart_date())) {
		        return -1;
		      }
		      else {
		        return d1.getStart_date().isAfter(d2.getStart_date()) ? 1 : 0;
		      }
		    }
		    else if (d1.getStart_date() != null && d2.getStart_date() == null) {
		      return -1;
		    }
		    else if (d1.getStart_date() == null && d2.getStart_date() != null) {
		      return 1;
		    }
		    else {
		      return 0;
		    }
	};

}
