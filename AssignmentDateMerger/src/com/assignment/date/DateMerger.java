package com.assignment.date;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DateMerger {

	public static void main(String[] args) {
		
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		
		
		 List<DateRange> dateRanges = new ArrayList<>();
		    dateRanges.add(new DateRange(LocalDate.of(2014, 1, 1), LocalDate.of(2014, 1, 30)));
		    dateRanges.add(new DateRange(LocalDate.of(2014, 1, 15), LocalDate.of(2014, 2, 15)));
		    dateRanges.add(new DateRange(LocalDate.of(2014, 3, 10), LocalDate.of(2014, 4, 15)));
		    dateRanges.add(new DateRange(LocalDate.of(2014, 4, 10), LocalDate.of(2014, 5, 15)));
		    
		    
		    System.out.println("## INPUT--->");
		    dateRanges.stream().forEach(dateRange -> System.out.println(dateRange.getStart_date().format(myFormatObj) + " - " + dateRange.getEnd_date().format(myFormatObj)));

		    List<DateRange> mergedDateRange = mergeDateRange(dateRanges);

		    System.out.println("## OUTPUT-->");
		    mergedDateRange.stream().forEach(dateRange -> System.out.println(dateRange.getStart_date().format(myFormatObj) + " - " + dateRange.getEnd_date().format(myFormatObj)));
		  }
	 private static List<DateRange> mergeDateRange(List<DateRange> dateRanges) {
		 Set<DateRange> mergedDateRangeSet = new HashSet<>();
		    Collections.sort(dateRanges, DateRange.start_date_Comparator);

		    mergedDateRangeSet.add(dateRanges.get(0));
		    for (int index = 1; index < dateRanges.size(); index++) {
		      DateRange current = dateRanges.get(index);
		      List<DateRange> toBeAdded = new ArrayList<>();
		      Boolean rangeMerged = false;
		      for (DateRange mergedRange : mergedDateRangeSet) {
		        DateRange merged = checkOverlap(mergedRange, current);
		        if (merged == null) {
		          toBeAdded.add(current);
		        }
		        else {
		          mergedRange.setEnd_date(merged.getEnd_date());
		          mergedRange.setStart_date(merged.getStart_date());
		          rangeMerged = true;
		          break;
		        }
		      }
		      if (!rangeMerged) {
		        mergedDateRangeSet.addAll(toBeAdded);
		      }
		      toBeAdded.clear();
		    }
		    List<DateRange> mergedDateRangeList = new ArrayList<>(mergedDateRangeSet);
		    Collections.sort(mergedDateRangeList, DateRange.start_date_Comparator);
		    return mergedDateRangeList;
		  }
	 private static DateRange checkOverlap(DateRange mergedRange, DateRange current) {
		    if (mergedRange.getStart_date().isAfter(current.getEnd_date()) || mergedRange.getEnd_date().isBefore(current.getStart_date())) {
		      return null;
		    }
		    else {
		      return new DateRange(mergedRange.getStart_date().isBefore(current.getStart_date()) ? mergedRange.getStart_date() : current.getStart_date(),
		        mergedRange.getEnd_date().isAfter(current.getEnd_date()) ? mergedRange.getEnd_date() : current.getEnd_date());
		    }
	 }
	

	

}
